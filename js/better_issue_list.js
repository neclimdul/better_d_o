
// For a project issue list find the table and process each row.
var issue_table = document.getElementsByClassName('project-issue');
for (var i = issue_table.length - 1; i >= 0; i--) {
  let rows = issue_table[i].getElementsByTagName('tr');
  for (var row = rows.length - 1; row >= 0; row--) {
    processIssueRow(rows[row]);
  }  
}

/**
 * Process issue queue row.
 */
function processIssueRow(row) {
  var cell,
    extra = document.createElement('div'),
    title_cell = row.getElementsByClassName('views-field views-field-title')[0];

  // Force title width to 100%.
  title_cell.style.width = '100%';
  
  // Setup our wrapper and attach it to the title cell.
  extra.className = 'bdo-title-extra';
  title_cell.appendChild(extra);

  var classes_to_remove = {
    'Priority': 'views-field-field-issue-priority',
    'Category': 'views-field-field-issue-category',
    'Component': 'views-field-field-issue-component',
    'Version': 'views-field-field-issue-version',
  };
  for (var key in classes_to_remove) {
    cell = row.getElementsByClassName(classes_to_remove[key])[0];
    // If we don't find the cell just skip processing. Maybe we're re-parsing or there is a change or its just a different layout somehow.
    if (typeof cell == 'undefined') {
      continue;
    }
    // If the row container normal cells(not heaader) do some additional processing to move contents.
    if (row.cells[0].nodeName == 'TD') {
      // Create field wrapper.
      let tmp = extra.appendChild(document.createElement('div'));

      // Add label.
      let label = document.createElement('strong');
      label.appendChild(document.createTextNode(key));
      tmp.appendChild(label)

      // Add field contents.
      tmp.appendChild(document.createTextNode(': ' + cell.innerHTML.trim()))
    }
    // Remove old cell.
    cell.remove();
  }
}
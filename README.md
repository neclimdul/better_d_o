# Better D.O

Extension for some in browser drupal.org improvements. Kinda like Dreditor but not. An emphasis on making browsing better.

I don't expect there will ever be a patch review tool, Dreditor already does that well.

## Features

Improve issue queue layout so titles are readable by moving some columns under the title.

## Notes

Just getting started. I assume there will be more cool things over time.